--TFA.AddFireSound( "Gun.Fire", "weapons/tfa_cso/gun/fire.wav", false, "^" )
--TFA.AddWeaponSound( "Gun.Reload", "weapons/tfa_cso/gun/reload.wav" )

--UMP45
TFA.AddFireSound( "Ump45.Fire", "weapons/tfa_cso/ump45/fire.wav", false, "^" )
TFA.AddWeaponSound( "Ump45.ClipIn", "weapons/tfa_cso/ump45/clipin.wav")
TFA.AddWeaponSound( "Ump45.ClipOut", "weapons/tfa_cso/ump45/clipout.wav")
TFA.AddWeaponSound( "Ump45.Boltslap", "weapons/tfa_cso/ump45/boltslap.wav")

--SG550
TFA.AddFireSound( "SG550.Fire", "weapons/tfa_cso/sg550/fire.wav", false, "^" )
TFA.AddWeaponSound( "SG550.Boltpull", "weapons/tfa_cso/sg550/boltpull.wav" )
TFA.AddWeaponSound( "SG550.ClipIn", "weapons/tfa_cso/sg550/clipin.wav" )
TFA.AddWeaponSound( "SG550.ClipOut", "weapons/tfa_cso/sg550/clipout.wav" )

--Famas
TFA.AddFireSound( "Famas.Fire", "weapons/tfa_cso/famas/fire1.wav", false, "^" )
TFA.AddWeaponSound( "Famas.Forearm", "weapons/tfa_cso/famas/forearm.wav" )
TFA.AddWeaponSound( "Famas.ClipIn", "weapons/tfa_cso/famas/clipin.wav" )
TFA.AddWeaponSound( "Famas.ClipOut", "weapons/tfa_cso/famas/clipout.wav" )

--Galil
TFA.AddFireSound( "Galil.Fire", "weapons/tfa_cso/galil/fire.wav", false, "^" )
TFA.AddWeaponSound( "Galil.ClipOut", "weapons/tfa_cso/galil/clipout.wav" )
TFA.AddWeaponSound( "Galil.ClipIn", "weapons/tfa_cso/galil/clipin.wav" )
TFA.AddWeaponSound( "Falil.BoltPull", "weapons/tfa_cso/galil/boltpull.wav" )

--QBB95
TFA.AddFireSound( "Qbb95.Fire", "weapons/tfa_cso/qbb95/fire.wav", false, "^" )
TFA.AddWeaponSound( "Qbb95.Draw", "weapons/tfa_cso/qbb95/draw.wav" )
TFA.AddWeaponSound( "Qbb95.ClipIn", "weapons/tfa_cso/qbb95/clipin.wav" )
TFA.AddWeaponSound( "Qbb95.ClipOut", "weapons/tfa_cso/qbb95/clipout.wav" )
TFA.AddWeaponSound( "Qbb95.ClipOn", "weapons/tfa_cso/qbb95/clipon.wav" )

--VSK94
TFA.AddFireSound( "VSK.Fire", "weapons/tfa_cso/vsk94/fire.wav", false, "^" )
TFA.AddWeaponSound( "VSK.Draw", "weapons/tfa_cso/vsk94/draw.wav" )
TFA.AddWeaponSound( "VSK.ClipIn", "weapons/tfa_cso/vsk94/clipin.wav" )
TFA.AddWeaponSound( "VSK.ClipOut", "weapons/tfa_cso/vsk94/clipout.wav" )

--Elite
TFA.AddFireSound( "Elite.Fire", "weapons/tfa_cso/elite/fire.wav", false, "^" )
TFA.AddWeaponSound( "Elite.Draw", "weapons/tfa_cso/elite/draw.wav")
TFA.AddWeaponSound( "Elite.Clipout", "weapons/tfa_cso/elite/clipout.wav")
TFA.AddWeaponSound( "Elite.LeftClipin", "weapons/tfa_cso/elite/leftclipin.wav")
TFA.AddWeaponSound( "Elite.RightClipin", "weapons/tfa_cso/elite/rightclipin.wav")
TFA.AddWeaponSound( "Elite.SlideRelease", "weapons/tfa_cso/elite/sliderelease.wav")
TFA.AddWeaponSound( "Elite.ReloadStart", "weapons/tfa_cso/elite/twirl.wav")

--AUG
TFA.AddFireSound( "AUG.Fire", "weapons/tfa_cso/aug/fire.wav", false, "^" )
TFA.AddWeaponSound( "AUG.ClipOut", "weapons/tfa_cso/aug/clipout.wav")
TFA.AddWeaponSound( "AUG.ClipIn", "weapons/tfa_cso/aug/clipin.wav")
TFA.AddWeaponSound( "AUG.Boltpull", "weapons/tfa_cso/aug/boltpull.wav")
TFA.AddWeaponSound( "AUG.Boltslap", "weapons/tfa_cso/aug/boltslap.wav")
TFA.AddWeaponSound( "AUG.Forearm", "weapons/tfa_cso/aug/forearm.wav")

--SG552
TFA.AddFireSound( "SG552.Fire", "weapons/tfa_cso/sg552/fire.wav", false, "^" )
TFA.AddWeaponSound( "SG552.ClipOut", "weapons/tfa_cso/sg552/clipout.wav")
TFA.AddWeaponSound( "SG552.ClipIn", "weapons/tfa_cso/sg552/clipin.wav")
TFA.AddWeaponSound( "SG552.Boltpull", "weapons/tfa_cso/sg552/boltpull.wav")

--Five-Seven
TFA.AddFireSound( "Fiveseven.Fire", "weapons/tfa_cso/fiveseven/fire.wav", false, "^" )
TFA.AddWeaponSound( "Fiveseven.ClipIn", "weapons/tfa_cso/fiveseven/clipin.wav")
TFA.AddWeaponSound( "Fiveseven.SlidePull", "weapons/tfa_cso/fiveseven/slidepull.wav")
TFA.AddWeaponSound( "Fiveseven.ClipOut", "weapons/tfa_cso/fiveseven/clipout.wav")
TFA.AddWeaponSound( "Fiveseven.SlideRelease", "weapons/tfa_cso/fiveseven/sliderelease.wav")

--M3
TFA.AddFireSound( "M3.Fire", "weapons/tfa_cso/m3/fire.wav", false, "^" )
TFA.AddWeaponSound( "M3.Pump", "weapons/tfa_cso/m3/pump.wav")
TFA.AddWeaponSound( "M3.Insert", "weapons/tfa_cso/m3/insert.wav")

--Mac-10
TFA.AddFireSound( "MAC10.Fire", "weapons/tfa_cso/mac10/fire.wav", false, "^" )
TFA.AddWeaponSound( "MAC10.ClipOut", "weapons/tfa_cso/mac10/clipout.wav")
TFA.AddWeaponSound( "MAC10.ClipIn", "weapons/tfa_cso/mac10/clipin.wav")
TFA.AddWeaponSound( "MAC10.ClipRelease", "weapons/tfa_cso/mac10/cliprelease.wav")
TFA.AddWeaponSound( "MAC10.Boltpull", "weapons/tfa_cso/mac10/boltpull.wav")

--P228
TFA.AddFireSound( "P228.Fire", "weapons/tfa_cso/p228/fire.wav", false, "^" )
TFA.AddWeaponSound( "P228.ClipIn", "weapons/tfa_cso/p228/clipin.wav")
TFA.AddWeaponSound( "P228.ClipOut", "weapons/tfa_cso/p228/clipout.wav")
TFA.AddWeaponSound( "P228.Deploy", "weapons/tfa_cso/p228/deploy.wav")
TFA.AddWeaponSound( "P228.SlidePull", "weapons/tfa_cso/p228/slidepull.wav")
TFA.AddWeaponSound( "P228.SlideRelease", "weapons/tfa_cso/p228/sliderelease.wav")

--SVD
TFA.AddFireSound( "SVD.Fire", "weapons/tfa_cso/svd/fire.wav", false, "^" )
TFA.AddWeaponSound( "SVD.ClipOut", "weapons/tfa_cso/svd/clipout.wav")
TFA.AddWeaponSound( "SVD.ClipIn", "weapons/tfa_cso/svd/clipin.wav")
TFA.AddWeaponSound( "SVD.Draw", "weapons/tfa_cso/svd/draw.wav")
TFA.AddWeaponSound( "SVD.ClipOn", "weapons/tfa_cso/svd/clipon.wav")

--K1A
TFA.AddFireSound( "K1A.Fire", "weapons/tfa_cso/k1a/fire.wav", false, "^" )
TFA.AddFireSound( "K1ASE.Fire", "weapons/tfa_cso/k1a/fire2.wav", false, "^" )
TFA.AddWeaponSound( "K1A.ClipOut", "weapons/tfa_cso/k1a/clipout.wav")
TFA.AddWeaponSound( "K1A.ClipIn", "weapons/tfa_cso/k1a/clipin.wav")
TFA.AddWeaponSound( "K1A.Foley1", "weapons/tfa_cso/k1a/foley1.wav")
TFA.AddWeaponSound( "K1A.Draw", "weapons/tfa_cso/k1a/draw.wav")

--Dual Deagle
TFA.AddFireSound( "DDE.Fire", "weapons/tfa_cso/ddeagle/fire.wav", false, "^" )
TFA.AddWeaponSound( "DDE.ClipIn", "weapons/tfa_cso/ddeagle/clipin.wav")
TFA.AddWeaponSound( "DDE.ClipOut", "weapons/tfa_cso/ddeagle/clipout.wav")
TFA.AddWeaponSound( "DDE.ClipOff", "weapons/tfa_cso/ddeagle/clipoff.wav")
TFA.AddWeaponSound( "DDE.Twirl", "weapons/tfa_cso/ddeagle/twirl.wav")
TFA.AddWeaponSound( "DDE.Load", "weapons/tfa_cso/ddeagle/load.wav")

--G3SG1
TFA.AddFireSound( "G3SG1.Fire", "weapons/tfa_cso/g3sg1/fire.wav", false, "^" )
TFA.AddWeaponSound( "G3SG1.ClipIn", "weapons/tfa_cso/g3sg1/clipin.wav")
TFA.AddWeaponSound( "G3SG1.ClipOut", "weapons/tfa_cso/g3sg1/clipout.wav")
TFA.AddWeaponSound( "G3SG1.Slide", "weapons/tfa_cso/g3sg1/slide.wav")

--SterlingBayonet
TFA.AddFireSound( "STBYT.Fire", "weapons/tfa_cso/sterlingbayonet/fire.wav", false, "^" )
TFA.AddFireSound( "STBYT.Knife1", "weapons/tfa_cso/sterlingbayonet/knife1.wav", false, "^" )
TFA.AddFireSound( "STBYT.Knife2", "weapons/tfa_cso/sterlingbayonet/knife2.wav", false, "^" )
TFA.AddWeaponSound( "STBYT.ClipIn", "weapons/tfa_cso/sterlingbayonet/clipin.wav")
TFA.AddWeaponSound( "STBYT.ClipOut", "weapons/tfa_cso/sterlingbayonet/clipout.wav")
TFA.AddWeaponSound( "STBYT.Boltpull", "weapons/tfa_cso/sterlingbayonet/boltpull.wav")
TFA.AddWeaponSound( "STBYT.Hit", "weapons/tfa_cso/sterlingbayonet/hit.wav")
TFA.AddWeaponSound( "STBYT.Stone1", "weapons/tfa_cso/sterlingbayonet/stone1.wav")
TFA.AddWeaponSound( "STBYT.Stone2", "weapons/tfa_cso/sterlingbayonet/stone2.wav")

--M134 Minigun
TFA.AddFireSound( "M134.Fire", "weapons/tfa_cso/m134/fire.wav", false, "^" )
TFA.AddWeaponSound( "M134.ClipOff", "weapons/tfa_cso/m134/clipoff.wav")
TFA.AddWeaponSound( "M134.ClipOn", "weapons/tfa_cso/m134/clipon.wav")
TFA.AddWeaponSound( "M134.PinPull", "weapons/tfa_cso/m134/pinpull.wav")
TFA.AddWeaponSound( "M134.Spinup", "weapons/tfa_cso/m134/spinup.wav")
TFA.AddWeaponSound( "M134.Spindown", "weapons/tfa_cso/m134/spindown.wav")