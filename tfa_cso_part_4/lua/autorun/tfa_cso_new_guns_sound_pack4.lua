--TFA.AddFireSound( "Gun.Fire", "weapons/tfa_cso/gun/fire.wav", false, "^" )
--TFA.AddWeaponSound( "Gun.Reload", "weapons/tfa_cso/gun/reload.wav" )

--Gunkata
TFA.AddFireSound( "Gunkata.Fire", "weapons/tfa_cso/gunkata/fire.wav", false, "^" )
TFA.AddFireSound( "Gunkata.Skill_Explode", "weapons/tfa_cso/gunkata/skill_last_exp.wav", false, "^" )
TFA.AddWeaponSound( "Gunkata.Reload", "weapons/tfa_cso/gunkata/reload.wav" )
TFA.AddWeaponSound( "Gunkata.Reload2", "weapons/tfa_cso/gunkata/reload2.wav" )
TFA.AddWeaponSound( "Gunkata.Draw2", "weapons/tfa_cso/gunkata/draw2.wav" )
TFA.AddWeaponSound( "Gunkata.Draw", "weapons/tfa_cso/gunkata/draw.wav" )
TFA.AddWeaponSound( "Gunkata.Idle", "weapons/tfa_cso/gunkata/idle.wav" )
TFA.AddWeaponSound( "Gunkata.Skill1", "weapons/tfa_cso/gunkata/skill_01.wav" )
TFA.AddWeaponSound( "Gunkata.Skill2", "weapons/tfa_cso/gunkata/skill_02.wav" )
TFA.AddWeaponSound( "Gunkata.Skill3", "weapons/tfa_cso/gunkata/skill_03.wav" )
TFA.AddWeaponSound( "Gunkata.Skill4", "weapons/tfa_cso/gunkata/skill_04.wav" )
TFA.AddWeaponSound( "Gunkata.Skill5", "weapons/tfa_cso/gunkata/skill_05.wav" )
TFA.AddWeaponSound( "Gunkata.Skilllast", "weapons/tfa_cso/gunkata/skill_last.wav" )
TFA.AddWeaponSound( "Gunkata.SkilllastExp", "weapons/tfa_cso/gunkata/skill_last_exp.wav" )
TFA.AddWeaponSound( "Gunkata.Hit1", "weapons/tfa_cso/gunkata/hit1.wav" )
TFA.AddWeaponSound( "Gunkata.Hit2", "weapons/tfa_cso/gunkata/hit2.wav" )

--Laserfist
TFA.AddFireSound( "Laserfist.FireA", "weapons/tfa_cso/laserfist/fire_a.wav", false, "^" )
TFA.AddFireSound( "Laserfist.FireB", "weapons/tfa_cso/laserfist/fire_b.wav", false, "^" )
TFA.AddWeaponSound( "Laserfist.ClipIn1", "weapons/tfa_cso/laserfist/clipin1.wav" )
TFA.AddWeaponSound( "Laserfist.ClipIn2", "weapons/tfa_cso/laserfist/clipin2.wav" )
TFA.AddWeaponSound( "Laserfist.ClipOut", "weapons/tfa_cso/laserfist/clipout.wav" )
TFA.AddWeaponSound( "Laserfist.Draw", "weapons/tfa_cso/laserfist/draw.wav" )
TFA.AddWeaponSound( "Laserfist.Idle", "weapons/tfa_cso/laserfist/idle.wav" )
TFA.AddWeaponSound( "Laserfist.Charge", "weapons/tfa_cso/laserfist/charge.wav" )
TFA.AddWeaponSound( "Laserfist.Empty_End", "weapons/tfa_cso/laserfist/shoot_empty_end" )
TFA.AddWeaponSound( "Laserfist.Empty_Loop", "weapons/tfa_cso/laserfist/shoot_empty_loop.wav" )
TFA.AddWeaponSound( "Laserfist.Boom", "weapons/tfa_cso/laserfist/shootb_exp.wav" )
TFA.AddWeaponSound( "Laserfist.Ready", "weapons/tfa_cso/laserfist/shootb_ready.wav" )
TFA.AddWeaponSound( "Laserfist.ShootB_Shoot", "weapons/tfa_cso/laserfist/shootb_shoot.wav" )
TFA.AddWeaponSound( "Laserfist.ShootB_Loop", "weapons/tfa_cso/laserfist/shootb_loop.wav" )

--Plasma Grenade
TFA.AddFireSound( "Sfnade.Boom", "weapons/tfa_cso/sfgrenade/boom.wav", false, "^" )
TFA.AddWeaponSound( "Sfnade.Pullpin", "weapons/tfa_cso/sfgrenade/pullpin.wav" )
TFA.AddWeaponSound( "Sfnade.Draw", "weapons/tfa_cso/sfgrenade/draw.wav" )
TFA.AddWeaponSound( "Sfnade.Ready", "weapons/tfa_cso/sfgrenade/ready.wav" )

--Dao Grenade
TFA.AddWeaponSound( "CartFrag.Pullpin", "weapons/tfa_cso/cartfrag/cartfrag.wav" )

--Frag Grenade
TFA.AddWeaponSound( "Fragnade.Pullpin", "weapons/tfa_cso/fragnade/pinpull.wav" )

--Pumpkin
TFA.AddFireSound( "Pumpkin.Boom", "weapons/tfa_cso/pumpkin/boom.wav", false, "^" )
TFA.AddWeaponSound( "Pumpkin.Pullpin", "weapons/tfa_cso/pumpkin/pullpin.wav" )
TFA.AddWeaponSound( "Pumpkin.Draw", "weapons/tfa_cso/pumpkin/draw.wav" )
TFA.AddWeaponSound( "Pumpkin.Throw", "weapons/tfa_cso/pumpkin/throw.wav" )

--Mooncake
TFA.AddFireSound( "Mooncake.Boom", "weapons/tfa_cso/mooncake/boom.wav", false, "^" )
TFA.AddWeaponSound( "Mooncake.Pullpin1", "weapons/tfa_cso/mooncake/pullpin1.wav" )
TFA.AddWeaponSound( "Mooncake.Pullpin2", "weapons/tfa_cso/mooncake/pullpin2.wav" )

--Gungnir
TFA.AddFireSound( "Gungnir.Shoot_Exp", "weapons/tfa_cso/gungnir/charge_shoot_exp.wav", false, "^" )
TFA.AddFireSound( "Gungnir.Shoot_1", "weapons/tfa_cso/gungnir/charge_shoot1.wav", false, "^" )
TFA.AddFireSound( "Gungnir.Shoot_Loop", "weapons/tfa_cso/gungnir/shoot_loop.wav", false, "^" )
TFA.AddFireSound( "Gungnir.End", "weapons/tfa_cso/gungnir/shoot_end.wav" )
TFA.AddWeaponSound( "Gungnir.Charge_loop", "weapons/tfa_cso/gungnir/charge_loop.wav" )
TFA.AddWeaponSound( "Gungnir.Charge_Shoot2", "weapons/tfa_cso/gungnir/charge_shoot2.wav" )
TFA.AddWeaponSound( "Gungnir.Shoot_B_Charge", "weapons/tfa_cso/gungnir/shoot_b_charge.wav" )
TFA.AddWeaponSound( "Gungnir.Draw", "weapons/tfa_cso/gungnir/draw.wav" )
TFA.AddWeaponSound( "Gungnir.Reload", "weapons/tfa_cso/gungnir/reload.wav" )
TFA.AddWeaponSound( "Gungnir.Idle", "weapons/tfa_cso/gungnir/idle.wav" )

--Thunder Pistol
TFA.AddFireSound( "Thunder.Shoot1", "weapons/tfa_cso/thunderpistol/shoot1.wav", false, "^" )
TFA.AddFireSound( "Thunder.Shoot", "weapons/tfa_cso/thunderpistol/shoot.wav", false, "^" )
TFA.AddFireSound( "Thunder.Exp", "weapons/tfa_cso/thunderpistol/exp.wav", false, "^" )
TFA.AddFireSound( "Thunder.Bigger_Exp", "weapons/tfa_cso/thunderpistol/bigger_exp.wav" )
TFA.AddWeaponSound( "Thunder.ClipOut", "weapons/tfa_cso/thunderpistol/clipout.wav" )
TFA.AddWeaponSound( "Thunder.Draw", "weapons/tfa_cso/thunderpistol/draw.wav" )
TFA.AddWeaponSound( "Thunder.Cloak", "weapons/tfa_cso/thunderpistol/cloak.wav" )
TFA.AddWeaponSound( "Thunder.ClipIn", "weapons/tfa_cso/thunderpistol/clipin.wav" )
TFA.AddWeaponSound( "Thunder.Idle1", "weapons/tfa_cso/thunderpistol/idle1.wav" )
TFA.AddWeaponSound( "Thunder.Idle2", "weapons/tfa_cso/thunderpistol/idle2.wav" )
TFA.AddWeaponSound( "Thunder.Idle3", "weapons/tfa_cso/thunderpistol/idle3.wav" )
TFA.AddWeaponSound( "Thunder.Loop", "weapons/tfa_cso/thunderpistol/idle_loop.wav" )

--Compound Bow
TFA.AddFireSound( "Bow.Shoot", "weapons/tfa_cso/bow/shoot.wav", false, "^" )
TFA.AddFireSound( "Bow.Charge_Shoot", "weapons/tfa_cso/bow/charge_shoot.wav", false, "^" )
TFA.AddWeaponSound( "Bow.ChargeStart1", "weapons/tfa_cso/bow/charge_start1.wav" )
TFA.AddWeaponSound( "Bow.ChargeStart2", "weapons/tfa_cso/bow/charge_start2.wav" )
TFA.AddWeaponSound( "Bow.ChargeFinish", "weapons/tfa_cso/bow/charge_finish.wav" )
TFA.AddWeaponSound( "Bow.Draw", "weapons/tfa_cso/bow/draw.wav" )

--Triple Tactical Knife
TFA.AddFireSound( "Tknife.Shoot", "weapons/tfa_cso/tknife/tknife_shoot1.wav", false, "^" )
TFA.AddFireSound( "Tknifeex.Shoot2", "weapons/tfa_cso/tknife/tknifeex_shoot2.wav", false, "^" )
TFA.AddWeaponSound( "Tknifeex2.Draw", "weapons/tfa_cso/tknife/tknifeex2_draw.wav" )
TFA.AddWeaponSound( "Tknife.Hitwall", "weapons/tfa_cso/tknife/hitwall.wav" )
TFA.AddWeaponSound( "Tknife.Hitwall2", "weapons/tfa_cso/tknife/hitwall2.wav" )

--Stinger
TFA.AddFireSound( "Stinger.Fire", "weapons/tfa_cso/stinger/fire.wav", false, "^" )
TFA.AddFireSound( "Stinger.Empty", "weapons/tfa_cso/stinger/empty.wav", false, "^" )
TFA.AddWeaponSound( "Stinger.Reload1", "weapons/tfa_cso/stinger/reload1.wav" )
TFA.AddWeaponSound( "Stinger.Reload2", "weapons/tfa_cso/stinger/reload2.wav" )
TFA.AddWeaponSound( "Stinger.Draw_Empty", "weapons/tfa_cso/stinger/draw_empty.wav" )
TFA.AddWeaponSound( "Stinger.Draw", "weapons/tfa_cso/stinger/draw.wav" )
