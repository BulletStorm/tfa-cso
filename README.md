# TFA: Counter-Strike: Online / Counter-Strike Nexon: Zombies SWEPs

In-Dev ports of Counter-Strike Online's weapons for TFA Base on Garry's Mod. Some are missing features and have noticable bugs.

## Workshop Download

* Part 1: https://steamcommunity.com/workshop/filedetails/?id=712848264
* Part 2: https://steamcommunity.com/sharedfiles/filedetails/?id=1309914309
* Part 3: https://steamcommunity.com/sharedfiles/filedetails/?id=1538229351
* Required: https://steamcommunity.com/workshop/filedetails/?id=415143062

It's highly recommended you use the workshop version. There might be incomplete, even broken guns on the git version.